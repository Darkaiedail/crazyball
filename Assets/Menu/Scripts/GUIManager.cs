﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;//using para dar acceso a los objetos nuevos de UI

public class GUIManager : MonoBehaviour {

	//Paneles
	public GameObject panelMenu;
	public GameObject panelProfile;

	public GameObject panelRegistro;
	public GameObject panelDatos;

	//campos texto solo view
	public Text campoUserNameData;
	public Text campoUserMailData;
	public Text campoUserScoreData;

	//Campos texto Input
	public InputField campoUserName;
	public InputField campoUserMail;

	//Datos de usuario
	private int userId = -1;
	private string userName;
	private string userMail;
	private float userScore;

	//aqui hacemos solo la gestion de los paneles
	void Start () {
		//esto borra las prefs
		//PlayerPrefs.DeleteAll ();

		//esto borra el tiempo
		//PlayerPrefs.SetFloat("userScore", 0.0f);

		//inicializacion de paneles
		panelProfile.transform.localScale = Vector3.zero;

		recuperarUsuarioPrefs ();

		if (userName != "") {//El usuario ya existe en preferencias
			panelDatos.SetActive(true);
			panelRegistro.SetActive(false);
		}
		else {//El usuario no se registro en prefs
			panelDatos.SetActive(false);
			panelRegistro.SetActive(true);
		}
	}
	
	void Update () {
	
	}

	public void pulsaBtnMenu(int numBtn){

		if (numBtn == 0) {//Jugar
			//Application.LoadLevel(2);//carga de nivel por hilo ppal
			StartCoroutine ("loadGameScene");
		} 
		else if (numBtn == 1) {//Profile
			//llamo a la funcion q me cierra todos los paneles
			cerrarPaneles();
			//hago un itween q me abra el panel prof llevando su escala a 1
			iTween.ScaleTo (panelProfile, iTween.Hash ("x", 1, "y", 1, "z", 1,
			                                           "easeType", "easeInOutExpo",
			                                           "time", .3,
			                                           "delay",.3));

			//gestion de subpaneles de profile
			if (userName != "") {//El usuario ya existe en preferencias
				panelDatos.SetActive(true);
				panelRegistro.SetActive(false);
			}
			else {//El usuario no se registro en prefs
				panelDatos.SetActive(false);
				panelRegistro.SetActive(true);
			}
		}
		else if (numBtn == 2) {//Scores
			
		}
	}

	//es llamada desde el btn backmenu del subpanel profile y desde el btn send
	public void volverAMenu(){
		//llamo a la funcion q me cierra todos los paneles
		cerrarPaneles();
		//hago un itween q me abra el panel prof llevando su escala a 1
		iTween.ScaleTo (panelMenu, iTween.Hash ("x", 1, "y", 1, "z", 1,
		                                           "easeType", "easeInOutExpo",
		                                           "time", .3,
		                                           "delay",.3));
	}

	void cerrarPaneles(){

		iTween.ScaleTo (panelProfile, iTween.Hash ("x", 0, "y", 0, "z", 0,
		                                           "easeType", "easeInOutExpo",
		                                           "time", .3));

		iTween.ScaleTo (panelMenu, iTween.Hash ("x", 0, "y", 0, "z", 0,
		                                        "easeType", "easeInOutExpo",
		                                        "time", .3));
	}

	//inicio un hilo secundario de precarga
	IEnumerator loadGameScene(){
		AsyncOperation async = Application.LoadLevelAsync (1);
		yield return async;
		//todas las lineas despues de este yield son ejecutadas cuando acabe la corrutina
		print("level cargado");
	}

	//esta funcion es llamada desde btn Send del submenu registro
	public void guardarUsuarioPrefs(){

		PlayerPrefs.SetInt ("userId", userId);//identificador en base datos externos, -1 si bool no se guardo
		PlayerPrefs.SetString("userName", campoUserName.text);//tomamos directamente el texto del input field
		PlayerPrefs.SetString("userMail", campoUserMail.text);//tomamos directamente el texto del input field
		PlayerPrefs.SetFloat("userScore", 0.0f);//establecemos un valor inicial a score

		recuperarUsuarioPrefs ();//actualizamos las variables de este script con los nuevos datos

		volverAMenu ();

		StartCoroutine ("guardarUsuarioEnServer");
	} 

	//inicio una corutina para enviar los datos al php y que este los lleve al server
	IEnumerator guardarUsuarioEnServer(){

		WWWForm form = new WWWForm ();

		form.AddField ("username", userName.ToString());//el texto entre " se tiene que llamar exactamente igual que la del documento php
		form.AddField ("mail", userMail.ToString());
		form.AddField ("score", userScore.ToString());
		form.AddField ("avatar", "no tiene avatar");

		WWW w = new WWW ("http://bishoport.com/crazyball/registro_usuario.php", form);

		yield return w;

		onConectionToServerComplete (w);
	}

	void onConectionToServerComplete (WWW w){

		PlayerPrefs.SetInt ("userId", int.Parse (w.text));
		print (PlayerPrefs.GetInt ("userId"));
	}

	private void recuperarUsuarioPrefs(){

		userId = PlayerPrefs.GetInt("userId");
		userName = PlayerPrefs.GetString ("userName");
		userMail = PlayerPrefs.GetString ("userMail");
		userScore = PlayerPrefs.GetFloat ("userScore");

		campoUserNameData.text = userName;
		campoUserMailData.text = userMail;

		//damos formato de tiempo
		float minutos = Mathf.Floor (userScore / 60);//saco los min dividiendo entre 60 y redondeando a la baja
		float segundos = (userScore % 60);//saco el resto de la division para sacar los seg
		
		string minutosStr = minutos.ToString("00");//le doy un formato inicial a la cadena de txt
		string segundosStr = segundos.ToString("00");

		campoUserScoreData.text = minutosStr + ":" + segundosStr;

		//hacemos la conexion con el servidor
		StartCoroutine("connectUpdateUserInServer");
	}

	IEnumerator connectUpdateUserInServer(){

		WWWForm form = new WWWForm ();

		form.AddField ("userId", PlayerPrefs.GetInt ("userId").ToString ());
		form.AddField ("userScore", PlayerPrefs.GetFloat ("userScore").ToString ());

		WWW w = new WWW ("http://bishoport.com/crazyball/actualiza_usuario.php", form);

		yield return w;

		print("Datos actualizados en usuario con id " + w.text);
	}
}
