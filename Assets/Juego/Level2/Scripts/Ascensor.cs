﻿using UnityEngine;
using System.Collections;

public class Ascensor : MonoBehaviour {

	void OnTriggerEnter(Collider obj){
		if (obj.gameObject.CompareTag ("Player")) {
			iTween.MoveTo(this.gameObject,iTween.Hash("position", new Vector3(29.99f,12.70f,-47.86f),
			                                          "time", 3.2f,
			                                          "delay", 1.0f));
		}
	}
}
