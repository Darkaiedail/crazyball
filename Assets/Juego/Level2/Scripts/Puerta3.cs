﻿using UnityEngine;
using System.Collections;

public class Puerta3 : MonoBehaviour {
	
	public GameObject puerta;
	
	void Start () {
		
	}
	
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider obj){
		if (obj.gameObject.CompareTag ("Player")) {
			iTween.MoveTo(puerta.gameObject,iTween.Hash("position", new Vector3(21.32f,20.02f,-108.02f),
			                                            "time", 3.0f));
		}
	}
	
	void OnTriggerExit(Collider obj){
		if (obj.gameObject.CompareTag ("Player")) {
			iTween.MoveTo(puerta.gameObject,iTween.Hash("position", new Vector3(21.32f,9.89f,-108.02f),
			                                            "time", 3.0f));
		}
	}
}
