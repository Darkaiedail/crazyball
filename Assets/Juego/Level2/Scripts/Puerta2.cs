﻿using UnityEngine;
using System.Collections;

public class Puerta2 : MonoBehaviour {
	
	public GameObject puerta;
	
	void Start () {
		
	}
	
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider obj){
		if (obj.gameObject.CompareTag ("Player")) {
			iTween.MoveTo(puerta.gameObject,iTween.Hash("position", new Vector3(14.13f,12.02f,-44.18f),
			                                            "time", 3.0f));
		}
	}
	
	void OnTriggerExit(Collider obj){
		if (obj.gameObject.CompareTag ("Player")) {
			iTween.MoveTo(puerta.gameObject,iTween.Hash("position", new Vector3(14.13f,2.53f,-44.18f),
			                                            "time", 3.0f));
		}
	}
}
