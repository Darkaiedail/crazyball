﻿using UnityEngine;
using System.Collections;

public class Pinchos : MonoBehaviour {
	
	public GameObject bolaPinchadaPref;

	public GameManager gameManager;

	void Update(){
	}

	void OnTriggerEnter (Collider obj){
		if (obj.gameObject.CompareTag ("Player")) {
			Instantiate (bolaPinchadaPref, obj.gameObject.transform.position, Quaternion.identity);
			Destroy (obj.gameObject);
			gameManager.SendMessage ("cuentaAtras");
		}
	}
}
