﻿using UnityEngine;
using System.Collections;

public class Bala : MonoBehaviour {

	public float speed = 10.0f;
//	public GameObject misil;

	void Update () {
//		GameObject bala = Instantiate (misil, this.transform.position, Quaternion.identity) as GameObject;

		//para que la bala se mueva en el eje z
		this.transform.Translate (Vector3.down * Time.deltaTime * speed);

//		Destroy (bala.gameObject, 0.05f);
		Destroy (this.gameObject,1.0f);
	}	
}
