﻿using UnityEngine;
using System.Collections;

public class Disparador : MonoBehaviour {
	
	public GameObject enemy;
	public Transform generatorPoint;
	
	private bool recargando = false;

	// funcion que hace que aparezcan los enemigos en pantalla mediante la func generarEnemigo
	void Update () {
		
		if (recargando != true) {
			generaEnemigo();
			StartCoroutine("recarga");
		}
	}
	
	//tiempo de retardo de generacion de enemigos
	IEnumerator recarga(){
		yield return new WaitForSeconds (1.0f);
		recargando = false;
	}
	
	//funcion para crear enemigos en la direccion de la nave
	void generaEnemigo(){
		recargando = true;
		Vector3 position = new Vector3 (generatorPoint.position.x,generatorPoint.position.y,generatorPoint.position.z);

		Quaternion rotacion = Quaternion.Euler (0, 0, 90);
		Instantiate (enemy, position, rotacion);		
	}

}
