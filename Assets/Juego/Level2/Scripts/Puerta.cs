﻿using UnityEngine;
using System.Collections;

public class Puerta : MonoBehaviour {

	public GameObject puerta;

	void Start () {
	
	}
	
	void Update () {
	
	}

	void OnTriggerEnter(Collider obj){
		if (obj.gameObject.CompareTag ("Player")) {
			iTween.MoveTo(puerta.gameObject,iTween.Hash("position", new Vector3(-15.02f,12.02f,-39.84f),
			                                            "time", 3.0f));
		}
	}

	void OnTriggerExit(Collider obj){
		if (obj.gameObject.CompareTag ("Player")) {
			iTween.MoveTo(puerta.gameObject,iTween.Hash("position", new Vector3(-15.02f,2.99f,-39.84f),
			                                            "time", 3.0f));
		}
	}
}
