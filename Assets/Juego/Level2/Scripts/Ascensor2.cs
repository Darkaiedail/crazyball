﻿using UnityEngine;
using System.Collections;

public class Ascensor2 : MonoBehaviour {

	void OnTriggerEnter(Collider obj){

		if (obj.gameObject.CompareTag ("Player")) {
			iTween.MoveTo(this.gameObject,iTween.Hash("position", new Vector3(-7.71f,19.60f,-51.87f),
			                                          "time", 3.2f,
			                                          "delay", 1.0f));
		}
	}
}
