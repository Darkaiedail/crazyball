﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BallController : MonoBehaviour {

	public bool useAccelerometer = false;
	public float force = 1.0f;
	public float jumpForce = 3.0f;

	public bool canJump = true;

	public GameObject particulasMoneda;

	private GameObject respawn;

	private GameObject gameManager;

	void Start () {
		respawn = GameObject.FindGameObjectWithTag("Respawn");
		gameManager = GameObject.FindGameObjectWithTag ("GameManager");
	}
	
	void Update () {

		Vector3 dir = Vector3.zero;

		if (useAccelerometer == false) {//sin acelerometro(mov)
			dir.x = Input.GetAxis("Horizontal");
			dir.z = Input.GetAxis("Vertical");
		} 
		else {//con acelerometro(mov)
			dir.x = Input.acceleration.x;
			dir.z = Input.acceleration.y;	
		}

		rigidbody.AddForce (dir * Time.deltaTime * force);
	}

	
	void OnCollisionEnter(Collision obj){
		if (obj.gameObject.CompareTag ("PlanoCollider")) {
//			reiniciarBola();
			gameManager.SendMessage("pararJuego");
		}
	}

	//esta funcion es llamada desde manager
	void reiniciarBola(){
		pararFisica ();
		this.transform.position = respawn.transform.position;
	}

	void pararFisica(){
		rigidbody.isKinematic = true;
	}

	void iniciarFisica(){
		rigidbody.isKinematic = false;
	}

	void OnTriggerEnter(Collider obj){
		if (obj.gameObject.CompareTag ("Moneda")) {
			Instantiate(particulasMoneda, obj.gameObject.transform.position, Quaternion.identity);
			Destroy(obj.gameObject);
			gameManager.SendMessage("recogeMonedas");
		}

		if (obj.gameObject.CompareTag ("Ascensor")) {
			iTween.MoveTo(this.gameObject,iTween.Hash("position", new Vector3(29.99f,13.44f,-47.86f),
			                                          "time", 2.7f,
			                                          "delay", 1.0f));
		}
		if (obj.gameObject.CompareTag ("Ascensor2")) {
			iTween.MoveTo(this.gameObject,iTween.Hash("position", new Vector3(-7.71f,20.3f,-51.87f),
			                                          "time", 2.7f,
			                                          "delay", 1.0f));
		}

		if (obj.gameObject.CompareTag ("CheckPoint")) {
			respawn.transform.position = obj.gameObject.transform.position;
			gameManager.SendMessage ("checkPoint");
		}

		if (obj.gameObject.CompareTag ("Finish")) {
			gameManager.SendMessage("llegadaMeta");		
		}
	}

	void OnTriggerExit(Collider obj){
		if (obj.gameObject.CompareTag ("CheckPoint")) {
			gameManager.SendMessage ("desactivarCheckPoint");
		}
	}
}
//edit-proyect settings-edit(unity remote 3)