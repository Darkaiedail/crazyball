﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	//Tiempo
	private float lapseTime;
	private bool timeRunning = false;
	public Text campoTiempo;

	//puntuacion moneda
	private int monedasRecogidas = 0;
	public int totalMonedas = 0;
	public Text campoMonedas;

	//cuenta atras
	public Text campoCuentaAtras;

	//ball
	public GameObject ballPrefab;
	private GameObject ball;
	public Transform respawn;

	//camera
	public GameObject camara;

	//panel final
	public GameObject panelFinal;

	public Text campoTiempoPF;
	public Text campoPenalizacionPF;
	public Text campoMonedasPF;
	public Text campoTotalPF;
	public Text campoRecordPF;

	public Text campoCheckPoint;

	void Start () {
		panelFinal.SetActive (false);
		StartCoroutine ("cuentaAtras");
	}

	IEnumerator cuentaAtras(){
		campoCuentaAtras.text = "3";
		yield return new WaitForSeconds (1);

		campoCuentaAtras.text = "2";
		yield return new WaitForSeconds (1);

		campoCuentaAtras.text = "1";
		yield return new WaitForSeconds (1);

		campoCuentaAtras.text = "";

		//inicia el juego
		ball = Instantiate (ballPrefab, respawn.position, Quaternion.identity) as GameObject;
		camara.SendMessage ("recibeTarget", ball.transform);
		timeRunning = true;
	}

	//esta funcion es llamada desde la bola cuando se sale de la pista
	void pararJuego(){
		Destroy (ball);
		timeRunning = false;

		StartCoroutine ("cuentaAtras");
	}

	//esta funcion es llamada desde la bola
	void recogeMonedas(){
		monedasRecogidas += 1;
		string monedasrecogidasString = monedasRecogidas.ToString("00");
		campoMonedas.text = monedasrecogidasString;
	}

	//esta funcion es llamada desde la bola
	void llegadaMeta(){
		timeRunning = false;

		//Formato tiempo(optimizar)
		float minutos = Mathf.Floor (lapseTime / 60);
		float segundos = (lapseTime % 60);

		string minutosStr = minutos.ToString("00");
		string segundosStr = segundos.ToString("00");

		campoTiempoPF.text = minutosStr + ":" + segundosStr;

		campoMonedasPF.text = monedasRecogidas + "/" + totalMonedas;

		int restaMonedas = totalMonedas - monedasRecogidas;

		//si hacemos una puntuacion mejor o es la primera vez q jugamos aqui se guarda el record
		float lastScore = PlayerPrefs.GetFloat("userScore");

		if (lastScore > lapseTime || lastScore == 0.0f) {
			PlayerPrefs.SetFloat("userScore", lapseTime);		
		}

//		float penalizacionTiempoSegundos = (restaMonedas * 5)/1000;
//
//		lapseTime += penalizacionTiempoSegundos;
//
//		//Formato tiempo(optimizar)
//		minutos = Mathf.Floor (lapseTime / 60);
//		segundos = (lapseTime % 60);
//
//		minutosStr = minutos.ToString("00");
//		segundosStr = segundos.ToString("00");
//
//		campoTotalPF.text = minutosStr + ":" + segundosStr;

		ball.SendMessage ("pararFisica");

		panelFinal.SetActive (true);
	}

	public void volverAMenuPrincipal(){
		StartCoroutine ("loadMenuScene");
	}

	IEnumerator loadMenuScene(){
		AsyncOperation async = Application.LoadLevelAsync (0);
		yield return async;
	}

	public void nextLevel(){
		StartCoroutine ("loadNextLevel");
	}

	IEnumerator loadNextLevel(){
		AsyncOperation async = Application.LoadLevelAsync (3);
		yield return async;
	}

	void Update () {
		if (timeRunning == true) {
			lapseTime += Time.deltaTime;
			cronometro();
		}
	}

	//es llamada desde update para q aparezca en pantalla los segundos
	void cronometro(){
		float minutos = Mathf.Floor (lapseTime / 60);//saco los min dividiendo entre 60 y redondeando a la baja
		float segundos = (lapseTime % 60);//saco el resto de la division para sacar los seg

		string minutosStr = minutos.ToString("00");//le doy un formato inicial a la cadena de txt
		string segundosStr = segundos.ToString("00");

		campoTiempo.text = minutosStr + ":" + segundosStr;
	}

	//estas dos funciones son llamadas desde ballController
	void checkPoint(){
		campoCheckPoint.text = "Check Point";
	}

	void desactivarCheckPoint(){
		campoCheckPoint.text = "";
	}

}
//void FixedUpdate(){}//funciona igual que update pero no hay que poner el time.deltatime
//void LateUpdate(){}//Funciona igual que update pero se ejecuta justo despues 